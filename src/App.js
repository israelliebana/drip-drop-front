//import logo from './logo.svg';
import './App.css'; 
import React from 'react';
import GardenList from './modules/gardenList.js';
import ZoneList from './modules/zoneList.js';
import ProgramList from './modules/programList.js';
import {getGardenList} from './components/gardenService.js';
import { getZoneList } from './components/zoneService';
import { getProgramList } from './components/programService';

class App extends React.Component {
  constructor(props){
    super(props);
    this.state= {
      gardenListData: [], 
      zoneListData: [],
      programListData: []
    };
  }

  componentDidMount(){
        getGardenList()
        .then(result => this.updateState(result))
        .catch(error => console.log("Error while getting gardens from API: " + error));
  
  }

   updateState= (gardens) => {
     const firstGarden= gardens[0];
     const firstZone= firstGarden.irrigationZones[0];

    this.setState({
      gardenListData: gardens,
      zoneListData: firstGarden.irrigationZones,
      programListData: firstZone.irrigationPrograms
    });
  }
  
  onClickGardenHandler= (gardenId) =>{
    const zones= this.getZones(gardenId);
    const firstZone= zones[0];
   
    this.setState({
      zoneListData: zones,
      programListData: firstZone.irrigationPrograms
    });
  }

  getZones= (gardenId) =>{
    let result= [];

    this.state.gardenListData.forEach( (garden) =>{
      if(garden.id === gardenId){
        result= garden.irrigationZones;
      }

    });

    return result;

  }

  getPrograms= (zoneId) => {
    let result= [];

    this.state.zoneListData.forEach( (zone) => {
      if(zone.id === zoneId){
        result= zone.irrigationPrograms;
      }
    });

    return result;
  }

  onClickZoneHandler= (zoneId) =>{
    const programs= this.getPrograms(zoneId);
    this.setState(
      {
        programListData: programs
      }
    );
  }

  render(){
    return (
      <div>
        <div>
          <GardenList gardenListData={this.state.gardenListData} onClickHandler={this.onClickGardenHandler}/>
        </div>
        <div>
          <ZoneList zoneListData={this.state.zoneListData} onClickHandler={this.onClickZoneHandler}/>
        </div>
        <div>
          <ProgramList programListData={this.state.programListData}/>
        </div>
      </div>
  );
 }
 
}

export default App;
