import React from 'react';
import Garden from './garden.js';

class GardenList extends React.Component{

    renderList(){
        if(typeof this.props.gardenListData !== 'undefined'){
         return(this.props.gardenListData.map(gardenData =>
                <Garden gardenData= {gardenData} onClickHandler={this.props.onClickHandler} />));
        }else{
            return null;
        }
    }

    render(){
        return(
        <div>
            <table>
                <thead>
                    <th>Name</th>
                    <th>Phone Number</th>
                    <th>Zones</th>
                </thead>
                <tbody>
                    {this.renderList()}
               </tbody>
            </table>
        </div>        
        );
    }
}

export default GardenList;