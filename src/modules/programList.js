import React from 'react';
import Program from './program';

class ProgramList extends React.Component{
    renderList(){
        if(typeof this.props.programListData !== 'undefined'){
            return (
                this.props.programListData.map(programData => <Program programData= {programData} />)
            );
        }else{
            return null;
        }
    }

    render(){
        return(
            <div>
                <table>
                    <thead>
                        <th>Start at</th>
                        <th>Duration (mins)</th>
                        <th>Week pattern</th>
                    </thead>
                    <tbody>
                        {this.renderList()}               
                    </tbody>
                </table>
            </div>
        );
    }
}


export default ProgramList;