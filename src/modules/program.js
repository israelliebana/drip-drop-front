import React from 'react';

class Program extends React.Component{

    render(){
        return(
            <tr key={this.props.programData.id}>
                <td>
                    {this.props.programData.startTime}
                </td>
                <td>
                    {this.props.programData.durationInMins}
                </td>
                <td>
                    {this.props.programData.weekPattern}
                </td>
            </tr>
        );
    }
}

export default Program;
