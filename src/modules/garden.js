import React from 'react';

class Garden extends React.Component{

    onClickHandler= () =>{
        this.props.onClickHandler(this.props.gardenData.id);
    }

    render(){
        const gardenData= this.props.gardenData;

        return (
         <tr key={gardenData.id} >
           <td>
             {gardenData.name}
           </td> 
           <td>
             {gardenData.phoneNumber}
           </td>
           <td>
               <button onClick={this.onClickHandler}>Details</button>
           </td>
         </tr>
        );
    }
}

export default Garden;