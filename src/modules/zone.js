import React from 'react';

class Zone extends React.Component{

    onClickHandler= ()=>{
        this.props.onClickHandler(this.props.zoneData.id);
    }

    render(){
        return(
            <tr key={this.props.zoneData.id}>
                <td>
                    {this.props.zoneData.name}
                </td>
                <td>
                    {this.props.zoneData.output}
                </td>
                <td>
                    <button onClick={this.onClickHandler}>Details</button>
                </td>
            </tr>
        );
    }
}

export default Zone;