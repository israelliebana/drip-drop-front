import React from 'react';
import Zone from './zone.js';

class ZoneList extends React.Component{

    renderList(){
        if(typeof this.props.zoneListData !== 'undefined'){
            return(
                this.props.zoneListData.map(zoneData => <Zone zoneData= {zoneData} onClickHandler={this.props.onClickHandler} />)
            );
        }else{
            return null;
        }
    }

    render(){
        return(
            <div>
                <table>
                    <thead>
                        <th>Name</th>
                        <th>EV output</th>
                        <th>Programs</th>
                    </thead>
                    <tbody>
                        {this.renderList()}
                    </tbody>
                </table>
            </div>     
        );
    }
}

export default ZoneList;



