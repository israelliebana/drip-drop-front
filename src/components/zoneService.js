const zoneListData1= [{id: "1", name: "zone1", output: "1"},
                       {id: "2", name: "zone2", output: "2"}
                      ];

const zoneListData2= [{id: "3", name: "zone3", output: "1"},
                      {id: "4", name: "zone4", output: "2"}
                      ];


let map= new Map();
map.set("1", zoneListData1);
map.set("2", zoneListData2);


function getZoneList(gardenId){
    return (map.get(gardenId));
}

export {getZoneList};