
import axios from 'axios';

/*const gardenListData= [{id: "1", name: "garden1", phoneNumber: "+34655289708"},
                       {id: "2", name: "garden2", phoneNumber: "+34696528060"},
                       {id: "3", name: "garden3", phoneNumber: "+34696528060"}
                          ];
*/

const BASE_URL="http://localhost:8080/api/v1/garden";

const getGardenList= async () => {
    const response= await axios.get(BASE_URL);
    return response.data;
}


export {getGardenList};