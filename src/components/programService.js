const programListData1= [
    {id: "1", name: "program1", startTime: "9:0", durationInMins: "5", weekPattern: "L,X"},
    {id: "2", name: "program2", startTime: "9:30", durationInMins: "3", weekPattern: "L,V"}
];

const programListData2= [
    {id: "1", name: "program1", startTime: "9:0", durationInMins: "5", weekPattern: "L,X"},
    {id: "2", name: "program2", startTime: "9:30", durationInMins: "3", weekPattern: "L,V"}
];

let programs= new Map();
programs.set("1", programListData1);
programs.set("2", programListData2);

function getProgramList(zoneId){
    return(programs.get(zoneId));
}

export {getProgramList};
